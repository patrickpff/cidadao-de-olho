<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deputado;
use App\Tipo_Despesa;
use App\Verbas_Indenizatorias;
use App\Tag_Localizacao;
use App\Tipo_Tag;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    //
	function __construct(){
		ini_set('max_execution_time', 5000);
		set_time_limit(5000);
	}

    public function index(Request $request){
    	$mais_gastaram = Deputado::withCount(['verbas_indenizatorias' => function($query) {
			    $query->select(DB::raw('sum(valor_reembolsado)'));
			}])->orderBy('verbas_indenizatorias_count', 'DESC')->take(5)->get();

        $partidos_mais_gastaram = Deputado::withCount(
            ['verbas_indenizatorias' => function($query) {
                $query->select(DB::raw('sum(valor_reembolsado)'));
            }])->get()->groupBy('partido')->sortByDesc(function($value, $key){
                return $value->sum('verbas_indenizatorias_count');
            })->take(5);

    	return view('home')->with([
    		'mais_gastaram' => $mais_gastaram,
            'partidos_mais_gastaram' => $partidos_mais_gastaram,
    	]);
    }

    public function sync (Request $request){

    	// Pega as tag_localizacao atraves da API
    	$array_tag_localizacao = json_decode(
    		file_get_contents('http://dadosabertos.almg.gov.br/ws/indexacao/tags/pesquisa?formato=json', true)
    	);
    	
    	// Salva a tag_localizacao
    	foreach($array_tag_localizacao->list as $item){

    		//Salvar o tipo_tag
    		$tipo_tag = Tipo_tag::where('id_api', $item->tipo->id)->first();

			if($tipo_tag == null){
				$tipo_tag = new Tipo_Tag();
			}
			
			$tipo_tag->nome = $item->tipo->nome;
			$tipo_tag->id_api = $item->tipo->id;

			$tipo_tag->save();

    		$tag_localizacao = Tag_Localizacao::where('id_api', $item->id)->first();

    		// Salvar a localização
    		if($tag_localizacao == null){
    			$tag_localizacao = new Tag_Localizacao();
    		}

    		$tag_localizacao->desc = $item->descricao;
			$tag_localizacao->assinaturaBoletim = $item->assinaturaBoletim;
			$tag_localizacao->assinaturaRSS = $item->assinaturaRss;
			$tag_localizacao->id_api = $item->id;
			$tag_localizacao->id_tipo_tag = $tipo_tag->id;

    		$tag_localizacao->save();
    	}

    	// Pega a lista de deputados através da API
    	$array_deputados = json_decode(
    		file_get_contents('http://dadosabertos.almg.gov.br/ws/deputados/em_exercicio?formato=json?mostraTags=true', true)
    	);

    	// Salva a lista de deputados
    	foreach($array_deputados->list as $item){

    		$deputado = Deputado::where('id_api', $item->id)->first();
    		
    		if ($deputado == null){
    			$deputado = new Deputado();
    		}

    		$deputado->nome = $item->nome;
    		$deputado->partido = $item->partido;
    		$deputado->id_api = $item->id;
    		$tag_localizacao = Tag_Localizacao::where('id_api', $item->tagLocalizacao)->first();
    		$deputado->id_tag_localizacao = $tag_localizacao->id;

    		$deputado->save();
    	}
		
		
    	$deputados = Deputado::all();
		// Salva a lista de verbas indenizadoras
    	foreach($deputados as $deputado){
			$array_data_verbas = json_decode(
	    		file_get_contents('http://dadosabertos.almg.gov.br/ws/prestacao_contas/verbas_indenizatorias/legislatura_atual/deputados/'. $deputado->id_api .'/datas?formato=json', true)
	    	);


	    	if($array_data_verbas->list){
	    		foreach($array_data_verbas->list as $data_verba_item){
    				foreach($data_verba_item->dataReferencia as $key => $value){
    					if($value != 'sql-timestamp'){
    						$data = Carbon::parse($value);

    						$array_data_verbas = json_decode(
					    		file_get_contents('http://dadosabertos.almg.gov.br/ws/prestacao_contas/verbas_indenizatorias/legislatura_atual/deputados/'. $deputado->id_api .'/'. $data->year .'/'.$data->month.'?formato=json', true)
					    	);

    						foreach($array_data_verbas->list as $tipo_despesa_value){
    							$tipo_despesa = Tipo_Despesa::where('id_api', $tipo_despesa_value->codTipoDespesa)->first();

    							if($tipo_despesa == null){
    								$tipo_despesa = new Tipo_Despesa();
    							}

    							$tipo_despesa->nome = $tipo_despesa_value->descTipoDespesa;
    							$tipo_despesa->id_api = $tipo_despesa_value->codTipoDespesa;

    							$tipo_despesa->save();


    							foreach($tipo_despesa_value->listaDetalheVerba as $detalhe){
    								$verba_indenizatoria = Verbas_Indenizatorias::where('id_api', $detalhe->id)->where('id_deputado', $deputado->id)->first();

    								if($verba_indenizatoria == null){
    									$verba_indenizatoria = new Verbas_Indenizatorias();
    								}

    								$verba_indenizatoria->nome_emitente = $detalhe->nomeEmitente;
    								if (isset($detalhe->descDocumento)){
    									$verba_indenizatoria->desc_documento = $detalhe->descDocumento;
    								}
    								$verba_indenizatoria->valor_despesa = $detalhe->valorDespesa;
    								$verba_indenizatoria->cpf_cnpj = $detalhe->cpfCnpj;
    								$verba_indenizatoria->valor_reembolsado = $detalhe->valorReembolsado;

				    				foreach($detalhe->dataReferencia as $key => $value){
				    					if($value != 'sql-timestamp'){
				    						$verba_indenizatoria->data_referencia = Carbon::parse($value)->format('Y-m-d');
				    					}
				    				}

				    				foreach($detalhe->dataEmissao as $key => $value){
				    					if($value != 'sql-timestamp'){
				    						$verba_indenizatoria->data_emissao = Carbon::parse($value)->format('Y-m-d');
				    					}
				    				}

    								$verba_indenizatoria->id_api = $detalhe->id;
    								$verba_indenizatoria->id_deputado = $deputado->id;
    								$verba_indenizatoria->id_tipo_despesa = $tipo_despesa->id;

    								$verba_indenizatoria->save();
    							}

    						}
    					}
	    			}
		    	}
	    	}
    	}

    	return redirect()->back();
    }

    public function list(Request $request){
        $mais_gastaram = Deputado::withCount(['verbas_indenizatorias' => function($query) {
                $query->select(DB::raw('sum(valor_reembolsado)'));
            }])->orderBy('verbas_indenizatorias_count', 'DESC');

        if($request->nome != ''){
            $mais_gastaram->where('nome', 'LIKE', '%'.$request->nome.'%');
        }

        if($request->partido != ''){
            $mais_gastaram->where('partido', '=', $request->partido);
        }

        $mais_gastaram = $mais_gastaram->paginate(15);

        $partidos = Deputado::select('partido as partido')->distinct()->get();

        return view('list')->with([
            'mais_gastaram' => $mais_gastaram,
            'partidos' => $partidos,
        ]);
    }

    public function detail (Request $request){
        $deputado = Deputado::find($request->id);

        return view('detail')->with([
            'deputado' => $deputado
        ]);
    }

    public function list_partidos(Request $request){
        $partidos_mais_gastaram = Deputado::withCount(
            ['verbas_indenizatorias' => function($query) {
                $query->select(DB::raw('sum(valor_reembolsado)'));
            }])->get()->groupBy('partido')->sortByDesc(function($value, $key){
                return $value->sum('verbas_indenizatorias_count');
            });
        
        return view('partidolist')->with([
            'partidos_mais_gastaram' => $partidos_mais_gastaram,
        ]);
    }

    public function deputados_partido(Request $request){
        $mais_gastaram = Deputado::where('partido', $request->partido)->withCount(['verbas_indenizatorias' => function($query) {
                $query->select(DB::raw('sum(valor_reembolsado)'));
            }])->orderBy('verbas_indenizatorias_count', 'DESC')->paginate(15);

        return view('list')->with([
            'mais_gastaram' => $mais_gastaram,
        ]);
    }
}
