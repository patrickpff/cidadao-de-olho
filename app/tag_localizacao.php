<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag_Localizacao extends Model
{
    //
    protected $table = 'tag_localizacao';
    protected $fillable = [
    	'desc',
    	'assinaturaBoletim',
    	'assinaturaRSS',
    	'id_api',
    	
    	'id_tipo_tag',
    ];
}
