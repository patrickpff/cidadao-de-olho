<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deputado extends Model
{
    //
    protected $table = 'deputado';
    protected $fillable = [
    	'nome',
    	'partido',
    	'id_api',
    	
    	'id_tag_localizacao',
    ];

    public function verbas_indenizatorias(){
    	return $this->hasMany('App\Verbas_Indenizatorias', 'id_deputado', 'id');
    }
}
