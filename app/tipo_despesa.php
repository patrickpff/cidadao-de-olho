<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Despesa extends Model
{
    //
    protected $table = 'tipo_despesa';
    protected $fillable = [
    	'nome',
    	'id_api',
    ];
}
