<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verbas_Indenizatorias extends Model
{
    //
    protected $table = 'verbas_indenizatorias';
    protected $fillable = [
    	'valor',
    	'nome_emitente',
    	'desc_documento',
    	'valor_despesa',
    	'cpf_cnpj',
    	'valor_reembolsado',
    	'data_referencia',
    	'data_emissao',
    	'id_api',
    	
    	'id_deputado',
    	'id_tipo_despesa',
    ];

    public function tipo_despesa(){
        return $this->hasOne('App\Tipo_Despesa', 'id', 'id_tipo_despesa');
    }
}
