<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Tag extends Model
{
    //
    protected $table = 'tipo_tag';
    protected $fillable = [
    	'nome',
    	'id_api',
    ];
}
