<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagLocalizacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_localizacao', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('desc')->nullable();
            $table->string('assinaturaBoletim')->nullable();
            $table->string('assinaturaRSS')->nullable();
            $table->string('id_api');
            
            $table->unsignedBigInteger('id_tipo_tag');
            $table->foreign('id_tipo_tag')->references('id')->on('tipo_tag');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_localizacao');
    }
}
