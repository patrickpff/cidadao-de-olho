<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerbasIndenizatoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verbas_indenizatorias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome_emitente')->nullable();
            $table->string('desc_documento')->nullable();
            $table->float('valor_despesa')->nullable();
            $table->string('cpf_cnpj')->nullable();
            $table->float('valor_reembolsado')->nullable();
            $table->date('data_referencia')->nullable();
            $table->date('data_emissao')->nullable();
            $table->integer('id_api');
            
            $table->unsignedBigInteger('id_deputado');
            $table->foreign('id_deputado')->references('id')->on('deputado');

            $table->unsignedBigInteger('id_tipo_despesa');
            $table->foreign('id_tipo_despesa')->references('id')->on('tipo_despesa');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verbas_indenizatorias');
    }
}
