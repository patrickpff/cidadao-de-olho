@extends('layouts.app')

@section('content')

<div class="container">
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<h5 class="card-header">
					<div class="row">
						<div class="col-md-8">
							Lista completa dos partidos
						</div>
						<div class="col-md-4 text-right">
							<a href="{{route('index')}}" class="btn btn-primary">Voltar</a>
						</div>
					</div>
				</h5>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped">
								<thead>
									<th>Partido</th>
									<th>Valor total reembolsado para todos os deputados do partido</th>
									<th>Média do valor reembolsado (Valor total/Quantidade de deputados)</th>
									<th></th>
								</thead>
								<tbody>
									@foreach($partidos_mais_gastaram as $partido => $deputados)
										<tr>
											<td>{{$partido}}</td>
											<td>R$ {{number_format($deputados->sum('verbas_indenizatorias_count'),2, ',', '.')}}</td>
											<td>R$ {{number_format($deputados->sum('verbas_indenizatorias_count')/$deputados->count(),2, ',', '.')}}</td>
											<td>
												<a href="{{route('deputados_partido', $partido)}}" class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="Listar detalhes"><i class="fas fa-th-list"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@section('scripts')
	<script type="text/javascript">
	    $('[data-toggle="tooltip"]').tooltip();
	</script>
@endsection

@endsection