@extends('layouts.app')

@section('content')

<div class="container">
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<h5 class="card-header">
					<div class="row">
						<div class="col-md-8">
							Lista de gastos declarados do deputado <label class="text-primary">{{$deputado->nome}}</label>
						</div>
						<div class="col-md-4 text-right">
							<a href="{{URL::previous()}}" class="btn btn-primary">Voltar</a>
						</div>
					</div>
				</h5>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped">
								<thead>
									<th>Nome do emitente</th>
									<th>Tipo de despesa</th>
									<th>Data de emissão</th>
									<th>Valor da despesa</th>
									<th>Valor reembolsado</th>
								</thead>
								<tbody>
									@foreach($deputado->verbas_indenizatorias as $verbas)
										<tr>
											<td>
												{{$verbas->tipo_despesa->nome}}
											</td>
											<td>
												{{$verbas->nome_emitente}}
											</td>
											<td>
												{{date('d/m/Y', strtotime($verbas->data_emissao))}}
											</td>
											<td>R$ {{number_format($verbas->valor_despesa, 2, ',', '.')}}</td>
											<td>R$ {{number_format($verbas->valor_reembolsado, 2, ',', '.')}}</td>
										</tr>
									@endforeach
									<tr class="table-warning">
										<td colspan="3"><b>Total</b></td>
										<td>
											<b>R$ {{number_format($deputado->verbas_indenizatorias()->sum('valor_despesa'), 2, ',', '.')}}
											</b>
										</td>
										<td>
											<b>R$ {{number_format($deputado->verbas_indenizatorias()->sum('valor_reembolsado'), 2, ',', '.')}}
											</b>
										</td>
									</tr>
								</tbody>
							</table>	
						</div>
					</div>
					{{-- <div class="row">
						<div class="col-md-12">
							{{$mais_gastaram->links()}}
						</div>
					</div> --}}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection