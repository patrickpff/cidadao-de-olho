@extends('layouts.app')

@section('content')

<div class="container">
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<h5 class="card-header">
					<div class="row">
						<div class="col-md-8">
							Lista completa
						</div>
						<div class="col-md-4 text-right">
							<a href="{{route('index')}}" class="btn btn-primary">Voltar</a>
						</div>
					</div>
				</h5>
				<div class="card-body">
					@if(isset($partidos))
						<form>
							<div class="row">
								<div class="col-md-6">
									<input type="text" name="nome" placeholder="Digite o nome completo" class="form-control">
								</div>
								<div class="col-md-5">
									<select class="form-control" name="partido">
										<option value="">Escolha um partido</option>
										@foreach($partidos as $partido)
											<option value="{{$partido->partido}}">{{$partido->partido}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-1 text-right">
									<button  class="btn btn-primary"><i class="fas fa-search"></i></button>
								</div>
							</div>
						</form><br>
					@endif
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped">
								<thead>
									<th>Nome</th>
									<th>Partido</th>
									<th>Valor reembolsado</th>
									<th></th>
								</thead>
								<tbody>
									@foreach($mais_gastaram as $deputado)
										<tr>
											<td>{{$deputado->nome}}</td>
											<td>{{$deputado->partido}}</td>
											<td>R$ {{number_format($deputado->verbas_indenizatorias_count, 2, ',', '.')}}</td>
											<td>
												<a href="{{route('detail', [$deputado->id])}}" class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="Listar detalhes"><i class="fas fa-th-list"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							{{$mais_gastaram->links()}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@section('scripts')
	<script type="text/javascript">
	    $('[data-toggle="tooltip"]').tooltip();
	</script>
@endsection

@endsection