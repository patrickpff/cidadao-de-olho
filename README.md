# Cidadao de olho

Desafio feito para o processo seletivo da Codificar Sistemas Tecnológicos

## Detalhes técnicos

	o PHP 7.3.10
	o Banco de dados MySQL
	o Laravel Framework 6.x
	o 

## Passos para a execução da aplicação

1 - Criar o banco de dados com o nome "cidadaodeolho"

2 - No console, rodar o comando: php artisan prepare

3 - Servir a aplicação

## Na aplicação

Para puxar as informações da API, basta clicar no botão "Clique aqui para sincronizar as informações" e aguardar. Como o volume de dados é um pouco grande, esta tarefa pode demorar alguns minutos.

Procurar: Redes sociais