<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');
Route::get('/sync', 'HomeController@sync')->name('sync');
Route::get('/list', 'HomeController@list')->name('list');
Route::get('/detail/{id}', 'HomeController@detail')->name('detail');

Route::get('/list_partidos', 'HomeController@list_partidos')->name('list_partidos');
Route::get('/deputados_partido/{partido}', 'HomeController@deputados_partido')->name('deputados_partido');
