<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('prepare', function () {
    
    $this->comment('Criando link simbólico para [public/storage]...');
    Artisan::call('storage:link');

    $this->comment('Executando migrations...');
    Artisan::call('migrate');

})->describe('Prepara a aplicação para a primeira execução');
